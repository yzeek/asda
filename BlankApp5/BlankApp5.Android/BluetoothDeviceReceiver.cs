﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BlankApp5;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using Android.Bluetooth;

namespace BlankApp5.Droid
{
    [BroadcastReceiver(Enabled = true, Exported = true)]
    public class BluetoothDeviceReceiver : BroadcastReceiver, IScanReceiver, IScanSender
    {
        List<DeviceListItem> _devices;

        public BluetoothDeviceReceiver()
        {
            _devices = new List<DeviceListItem>();

        }

        public override void OnReceive(Context context, Intent intent)
        {
        
            switch (intent.Action)
            {
                case BluetoothAdapter.ActionDiscoveryFinished:
                    HandleDiscoveryFinished();
                    break;
                case "com.checkmate.found":
                    HandleDeviceFoundIntent(intent);
                    break;

            }
            
        }

        private void HandleDeviceFoundIntent(Intent intent)
        {
            BluetoothDevice newDevice = (BluetoothDevice)intent.GetParcelableExtra(BluetoothDevice.ExtraDevice);

            var device = new DeviceListItem
            {
                Name = newDevice.Name,
                Mac = newDevice.Address
            };

            if (!_devices.Any(d => d.Mac == device.Mac))
            {
                _devices.Add(device);
                MessagingCenter.Send<IScanReceiver, List<DeviceListItem>>(this, "btScanner", _devices);
            }
        }

        private void HandleDiscoveryFinished()
        {
            var bta = BluetoothAdapter.DefaultAdapter;
            foreach (var device in bta.BondedDevices)
            {
                if (!_devices.Any(d => d.Mac == device.Address))
                    _devices.Add(new DeviceListItem
                    {
                        Mac = device.Address,
                        Name = device.Name
                    });
            }
            MessagingCenter.Send<IScanSender, string>(this, "btScanner", "ended");
            MessagingCenter.Send<IScanReceiver, List<DeviceListItem>>(this, "btScanner", _devices);
        }
    }
}