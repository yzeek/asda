﻿using Android.App;
using Android.Bluetooth;
using Android.Bluetooth.LE;
using Android.Content;
using Android.Support.V4.App;
using Xamarin.Forms;

namespace BlankApp5.Droid
{
    public class LeScanCallback : ScanCallback, IScanSender
    {

        public override void OnScanResult(ScanCallbackType callbackType, ScanResult result)
        {
            base.OnScanResult(callbackType, result);
            Intent i = new Intent(BluetoothDevice.ActionFound);
            var device = result.Device;
            if (device != null)
            {
              //  MessagingCenter.Send<IScanSender, Android.Bluetooth.BluetoothDevice>(this, "btScanner", device);
            }


        }

    }
}