﻿using Android;
using Android.App;
using Android.Bluetooth;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Plugin.BLE;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace BlankApp5.Droid
{
    [Activity(Label = "BlankApp5",
        Icon = "@mipmap/ic_launcher",
        Theme = "@style/MainTheme",
        ScreenOrientation = ScreenOrientation.Portrait,
        MainLauncher = true,
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity, IScanReceiver
    {

        private BluetoothAdapter _btAdapter;

        private string[] permissionsArr = new string[] {
              Manifest.Permission.Bluetooth,
              Manifest.Permission.BluetoothAdmin,
              Manifest.Permission.BluetoothPrivileged,
              Manifest.Permission.AccessCoarseLocation,
              Manifest.Permission.AccessFineLocation };

        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new App(new AndroidInitializer()));

            if ((int)Build.VERSION.SdkInt > 23)
            {
                RequestPermissions(permissionsArr, 0);
        }
    }

    protected override void OnResume()
    {
        _btAdapter = BluetoothAdapter.DefaultAdapter;
        if (!_btAdapter.IsEnabled) _btAdapter.Enable();
        SubscribeScanMesannger();
        RegisterBTReceiver();

        base.OnResume();
    }

    private void RegisterBTReceiver()
    {

        BluetoothDeviceReceiver bluetoothDeviceReceiver = new BluetoothDeviceReceiver();
        IntentFilter filter = new IntentFilter();
        filter.AddAction(BluetoothDevice.ActionFound);
        filter.AddAction("com.checkmate.found");
        filter.AddAction(BluetoothAdapter.ActionDiscoveryFinished);

        RegisterReceiver(bluetoothDeviceReceiver, filter);
    }

    /// <summary>
    /// recieves the message from the search button click
    /// </summary>
    private void SubscribeScanMesannger()
    {

        MessagingCenter.Subscribe<IScanSender, string>(this, "btScanner", (sender, arg) =>
        {
            switch (arg)
            {
                case "start":
                    StartBleScan();
                    if (!_btAdapter.IsDiscovering)
                        _btAdapter.StartDiscovery();

                    break;
            }

        });

    }

    private async void StartBleScan()
    {

        var ble = CrossBluetoothLE.Current;
        var adapter = CrossBluetoothLE.Current.Adapter;

        List<BluetoothDevice> deviceList = new List<BluetoothDevice>();
        adapter.DeviceDiscovered += (s, a) => deviceList.Add(a.Device.NativeDevice as BluetoothDevice);

        if (!adapter.IsScanning)
            await adapter.StartScanningForDevicesAsync();

        foreach (BluetoothDevice d in deviceList)
        {
            if (d == null) continue;

            Intent i = new Intent("com.checkmate.found");
            i.PutExtra(BluetoothDevice.ExtraDevice, d);
            SendBroadcast(i);

        }

    }

}
}

