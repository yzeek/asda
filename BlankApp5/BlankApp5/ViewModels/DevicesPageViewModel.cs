﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Xamarin.Forms;

namespace BlankApp5.ViewModels
{
    public class DevicesPageViewModel : INotifyPropertyChanged
    {
        public DevicesPageViewModel()
        {
            BondedDevices = new List<DeviceListItem>();
            SubscribeDevicesFound();
            SubscribeScanStatusUpdates();
        }

        private void SubscribeScanStatusUpdates()
        {
            MessagingCenter.Subscribe<IScanSender, string>(this, "btScanner", (sender, arg) =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    IsBusy = arg == "start" ? true : false;
                });

                OnPropertyChanged("IsBusy");

            });
        }

        private void SubscribeDevicesFound()
        {
            MessagingCenter.Subscribe<IScanReceiver, List<DeviceListItem>>(this, "btScanner", (sender, arg) =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    BondedDevices = arg;
                    IsBusy = false;
                });

                OnPropertyChanged("BondedDevices");

            });
        }

        public List<DeviceListItem> BondedDevices { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;
        public bool IsBusy { get; set; }
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
