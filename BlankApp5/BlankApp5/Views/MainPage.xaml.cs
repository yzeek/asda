﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace BlankApp5.Views
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        protected async override void OnAppearing()
        {
            await Task.Delay(2000);
            await ContentWrapper.FadeTo(0, 1000);
            await Navigation.PushAsync(new DevicesPage());

            base.OnAppearing();
        }
    }
}