﻿using Android.Bluetooth;
using Xamarin.Forms;
namespace BlankApp5.Views
{
    public partial class DevicesPage : ContentPage, IScanSender
    {

        public DevicesPage()
        {
            InitializeComponent();
        }

        private void Button_Clicked(object sender, System.EventArgs e)
        {
            SendStartScanMessage();
        }

        private void SendStartScanMessage()
        {
            MessagingCenter.Send<IScanSender, string>(this, "btScanner", "start");
        }

        // prevent navigating to splash
        protected override bool OnBackButtonPressed()
        {
            return true;
        }

        private async void ListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            var selectedDevice = e.Item as DeviceListItem;
            string msg = string.Format("You have clicked {0} it's address is {1}", string.IsNullOrWhiteSpace(selectedDevice.Name)? "Unnamed device": selectedDevice.Name, selectedDevice.Mac);

            await DisplayAlert("Item selected ", msg, "ok");
        }
    }
}
